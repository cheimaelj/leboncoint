//
//  ProductsListViewModelTests.swift
//  LeboncointTests
//
//  Created by celj on 19/04/2023.
//

import XCTest
@testable import Leboncoint

class ProductsListViewModelTests: XCTestCase {

    var productListViewModel = ProductsListViewModel()
    
    override func setUp() {
        super.setUp()
        
        productListViewModel.products = getProductsMock()
        productListViewModel.categorys = getCategorysMock()
    }
    
    func testGetProducts(){
        productListViewModel.products = getProductsMock()
        let product1 = productListViewModel.getProducts(at: 0)
        let product2 = productListViewModel.getProducts(at: 10)
        let product3 = productListViewModel.getProducts(at: -5)

        XCTAssert(product1 != nil )
        XCTAssert(product1?.title == "product1")
        
        XCTAssert(product2 == nil )
        XCTAssert(product3 == nil )
    }
    
    func testGetCategorys(){
        productListViewModel.categorys = getCategorysMock()
        let category1 = productListViewModel.getCategorys(at: 0)
        let category2 = productListViewModel.getCategorys(at: 12)
        let category3 = productListViewModel.getCategorys(at: -1)

        XCTAssert(category1 != nil )
        XCTAssert(category1?.id == 1)
        
        XCTAssert(category2 == nil )
        XCTAssert(category3 == nil )
    }
    
    func testRefreshProductsData(){
        productListViewModel.refeshProducts(with: getProductsMock(), error: nil)
        XCTAssert(productListViewModel.products.count == getProductsMock().count)
    }
    
    func testRefreshCategorysData(){
        productListViewModel.refeshCategorys(with: getCategorysMock(), error: nil)
        XCTAssert(productListViewModel.categorys.count == getCategorysMock().count)
    }
    
}

extension ProductsListViewModelTests {
    
    func getCategorysMock() -> [CategoryProduct] {

        let categoryt1 = CategoryProduct.init(id: 1, name: "categoryt1")
        let categoryt2 = CategoryProduct.init(id: 2, name: "categoryt2")
        let categoryt3 = CategoryProduct.init(id: 3, name: "categoryt3")
        let categoryt4 = CategoryProduct.init(id: 4, name: "categoryt4")

        return [categoryt1, categoryt2, categoryt3, categoryt4]
    }
    
    func getProductsMock() -> [Product] {
        
        let product1 = Product(id: 1, category_id: 2, title: "product1", description: "product1 description", price: 32, images_url: Images_url(), creation_date: "2020-11-05T15:56:32+0000", is_urgent: false)
        
        let product2 = Product(id: 1, category_id: 2, title: "product2", description: "product2 description", price: 65, images_url: Images_url(), creation_date: "2020-07-05T15:56:32+0000", is_urgent: true)
        
        let product3 = Product(id: 1, category_id: 2, title: "product3", description: "product2 description", price: 98, images_url: Images_url(), creation_date: "2020-17-05T15:56:32+0000", is_urgent: false)
        
        return [product1, product2, product3]
    }
    
    
   
}
