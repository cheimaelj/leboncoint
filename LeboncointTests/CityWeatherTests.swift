//
//  CityWeatherTests.swift
//  LeboncointTests
//
//  Created by celj on 19/04/2023.
//

import XCTest
@testable import Leboncoint

class AppCoordinatorTests: XCTestCase {
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testWindowIsKey() {
        let window = WindowStub()
        let coordinator = AppCoordinator(window: window)
        coordinator.start()
        XCTAssertTrue(window.makeKeyAndVisibleCalled)
        XCTAssertNotNil(window.rootViewController)
    }
}

private class WindowStub: UIWindow {
    var makeKeyAndVisibleCalled = false
    override func makeKeyAndVisible() {
        makeKeyAndVisibleCalled = true
    }
}
