//
//  ImageViewController.swift
//  Leboncoint
//
//  Created by celj on 23/04/2023.
//

import UIKit



class ImageViewController: UIViewController, UIScrollViewDelegate {
    
    var imageView = UIImageView()
    var scrollView = UIScrollView()
    var viewModel: ImageViewModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        scrollView.delegate = self
        scrollView.minimumZoomScale = 1.0
        scrollView.maximumZoomScale = 5.0
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        
        imageView = convertUrlToImage(url: viewModel?.image ?? "")
        imageView.contentMode = .scaleAspectFit
        
        scrollView.addSubview(imageView)
        view.addSubview(scrollView)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        scrollView.frame = view.bounds
        imageView.frame = scrollView.bounds
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
}
