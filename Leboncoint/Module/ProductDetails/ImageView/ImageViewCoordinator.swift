//
//  ImageViewCoordinator.swift
//  Leboncoint
//
//  Created by celj on 23/04/2023.
//

import Foundation
import UIKit

class ImageViewCoordinator: Coordinator {
    
    var childCoordinators: [Coordinator] = []
    var parentCoordinator: Coordinator?
    private let image: String
    private var navigationController : UINavigationController
        
    init(navigationController: UINavigationController , image: String) {
        self.navigationController = navigationController
        self.image = image
    }
    
    func start() {
        let viewController = ImageViewController()
        let viewModel = ImageViewModel(image: image)
        viewController.viewModel = viewModel
        viewController.viewModel?.coordinator = self
        
        self.navigationController.pushViewController(viewController, animated: true)
        
    }
    
    func productDetailsDidFinish() {
        parentCoordinator?.childDidFinish(childCoordinator: self)
    }
}
