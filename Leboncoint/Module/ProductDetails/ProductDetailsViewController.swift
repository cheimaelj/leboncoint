//
//  ProductDetailsViewController.swift
//  Leboncoint
//
//  Created by celj on 17/04/2023.
//

import UIKit

class ProductDetailsViewController: UIViewController {
    
    
    // MARK: - Properties
    
    private let scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.showsVerticalScrollIndicator = false
        return scrollView
    }()
    
    private let productDetailView = ProductDetailView()
    var viewModel: ProductDetailsViewModel?
    
    
    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .backgroundColor
        title = "product_detail".localized
        guard let product = self.viewModel?.product else { return }
        productDetailView.configure(with: product)
        productDetailView.delegate = self
        setupSubviews()
        setupConstraints()
    }
    
    // MARK: - Private Functions
    
    private func setupSubviews() {
        scrollView.addSubview(productDetailView)
        view.addSubview(scrollView)
    }
    
    private func setupConstraints() {
        // Set up Auto Layout constraints for the subviews
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        productDetailView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            
            productDetailView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            productDetailView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            productDetailView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            productDetailView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
            productDetailView.widthAnchor.constraint(equalTo: scrollView.widthAnchor),
        ])
    }
    
}

extension ProductDetailsViewController : ProductDetailDelegate{
    func showImage(with image: String) {
        self.viewModel?.showImage()
    }
}
