//
//  ProductDetailView.swift
//  Leboncoint
//
//  Created by celj on 21/04/2023.
//

import UIKit

protocol ProductDetailDelegate: AnyObject {
    func showImage(with image: String)
}

class ProductDetailView: UIView {
    
    // MARK: - Properties
    
    private var  imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = .backgroundColor
        return imageView
    }()
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 16, weight: .bold)
        label.numberOfLines = 0
        return label
    }()
    
    private let descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.numberOfLines = 0
        return label
    }()
    
    private let dateLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = .gray
        return label
    }()
    
    private let priceLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.textColor = .label
        label.textAlignment = .center
        return label
    }()
    
    private let urgentLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.textColor = .backgroundColor
        label.textAlignment = .center
        label.backgroundColor = .red
        label.layer.cornerRadius = 4
        label.clipsToBounds = true
        return label
    }()
    
    // MARK: - Initializers
    
    weak var delegate: ProductDetailDelegate?
    var imageUrl: String = ""
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSubviews()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Public Functions
    
    func configure(with product: Product) {
        titleLabel.text = product.title
        descriptionLabel.text = product.description
        
        dateLabel.text = convertDateFormater(date: product.creation_date ?? "")
        
        // setup priceLabel
        priceLabel.numberOfLines = 2
        
        let firstAttributes: [NSAttributedString.Key: Any] = [
            .font: UIFont.systemFont(ofSize: 14),
            .foregroundColor: UIColor.label
        ]
        let firstLine = NSMutableAttributedString(string: "Prix\n", attributes: firstAttributes)
        
        let secondAttributes: [NSAttributedString.Key: Any] = [
            .font: UIFont.boldSystemFont(ofSize: 20),
            .foregroundColor: UIColor(named: "AccentColor") ?? .label
        ]
        let secondLine = NSAttributedString(string: String(product.price ?? 0) + " €", attributes: secondAttributes)
        
        let attributedText = NSMutableAttributedString()
        attributedText.append(firstLine)
        attributedText.append(secondLine)
        
        priceLabel.attributedText = attributedText
        
        if product.is_urgent ?? false {
            urgentLabel.isHidden = false
            urgentLabel.text = "urgent".localized
        } else {
            urgentLabel.isHidden = true
        }
        
        if let imageURL = URL(string: product.images_url?.thumb ?? ""){
            URLSession.shared.dataTask(with: imageURL) { data, response, error in
                DispatchQueue.main.async { [self] in
                    if let data = data, let image = UIImage(data: data) {
                        // if image exists we can display image view
                        self.imageView.image = image
                        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.imageViewTapped))
                        imageView.isUserInteractionEnabled = true
                        imageView.addGestureRecognizer(tapGesture)
                }else{
                    self.imageView.contentMode = .scaleAspectFit
                    self.imageView.image = UIImage(named: "default-img")
                }
            }
            }.resume()
            
        }
        
        imageUrl  = product.images_url?.small ?? ""
        
    }
    
    @objc func imageViewTapped(_ sender: UITapGestureRecognizer) {
        delegate?.showImage(with: imageUrl)
        
    }
    
    // MARK: - Private Functions
    
    private func setupSubviews() {
        addSubview(imageView)
        addSubview(titleLabel)
        addSubview(descriptionLabel)
        addSubview(dateLabel)
        addSubview(priceLabel)
        addSubview(urgentLabel)
    }
    
    private func setupConstraints() {
        imageView.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        dateLabel.translatesAutoresizingMaskIntoConstraints = false
        priceLabel.translatesAutoresizingMaskIntoConstraints = false
        urgentLabel.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            
            // Image view constraints
            imageView.topAnchor.constraint(equalTo: topAnchor, constant: 8),
            imageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8),
            imageView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8),
            imageView.heightAnchor.constraint(equalToConstant: 250),
            
            // title label constraints
            titleLabel.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 16),
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -120),
            
            // Price label constraints
            priceLabel.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 10),
            priceLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8),
            priceLabel.widthAnchor.constraint(equalToConstant: 110),
            
            // Description label constraints
            descriptionLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 30),
            descriptionLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            descriptionLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            
            // Date label constraints
            dateLabel.topAnchor.constraint(equalTo: descriptionLabel.bottomAnchor, constant: 8),
            dateLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            dateLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            
            // Urgent label constraints
            urgentLabel.topAnchor.constraint(equalTo: dateLabel.bottomAnchor, constant: 8),
            urgentLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            urgentLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10),
            urgentLabel.heightAnchor.constraint(equalToConstant: 20),
            urgentLabel.widthAnchor.constraint(equalToConstant: 100)
            
            
        ])
    }
}
