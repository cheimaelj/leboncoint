//
//  ProductsListCoordinator.swift
//  Leboncoint
//
//  Created by celj on 17/04/2023.
//

import Foundation
import UIKit

class ProductsListCoordinator: Coordinator {
    var childCoordinators: [Coordinator] = []
    private var navigationController : UINavigationController
        
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let viewController = ProductsListViewController()
        let viewModel = ProductsListViewModel()
        viewController.viewModel = viewModel
        viewController.viewModel?.coordinator = self
        
        self.navigationController.setViewControllers([viewController], animated: false)
        
    }
    
    func showProductDetailsScreen(product: Product, category: CategoryProduct) {
        let productDetailsCoordinator = ProductDetailsCoordinator(navigationController: navigationController, product: product, category: category)
        productDetailsCoordinator.parentCoordinator = self
        productDetailsCoordinator.start()
        childCoordinators.append(productDetailsCoordinator)
    }
    
   
}
