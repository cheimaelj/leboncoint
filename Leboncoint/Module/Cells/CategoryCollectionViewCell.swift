//
//  CategoryCollectionViewCell.swift
//  Leboncoint
//
//  Created by celj on 17/04/2023.
//

import UIKit

class CategoryCollectionViewCell: UICollectionViewCell {
    
    // MARK: - Properties
    
    private let categoryLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 16)
        
        label.textColor = .label
        label.numberOfLines = 2
        label.textAlignment = .center
        label.layer.borderWidth = 0.5
        label.layer.borderColor = UIColor.label.cgColor
        label.layer.cornerRadius = 15
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    // MARK: - Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Public Methods
    
    func configure(with category: CategoryProduct){
        categoryLabel.text = category.name
    }
    
    func selectedItem(){
        categoryLabel.layer.borderWidth = 2
        categoryLabel.font = UIFont.systemFont(ofSize: 16,weight: .bold)
        categoryLabel.layer.borderColor = UIColor(named: "AccentColor")?.cgColor
        categoryLabel.textColor = UIColor(named: "AccentColor")
    }
    
    func deselectItem(){
        categoryLabel.layer.borderWidth = 0.7
        categoryLabel.font = UIFont.systemFont(ofSize: 16)
        categoryLabel.layer.borderColor = UIColor.label.cgColor
        categoryLabel.textColor = .label
    }
    
    // MARK: - Private Methods
    
    private func setupViews() {
        backgroundColor = .backgroundColor
        
        addSubview(categoryLabel)
        
        NSLayoutConstraint.activate([
            categoryLabel.topAnchor.constraint(equalTo: topAnchor),
            categoryLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            categoryLabel.trailingAnchor.constraint(equalTo: trailingAnchor),
            categoryLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -2)
            
        ])
    }
    
}

