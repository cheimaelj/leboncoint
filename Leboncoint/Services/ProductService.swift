//
//  ProductService.swift
//  Leboncoint
//
//  Created by celj on 14/04/2023.
//

import Foundation

struct ProductService{
    
    func fetchProducts(completion: @escaping (Result<[Product] , NSError >)->()) {
        
        
        guard let url = URL(string: "https://raw.githubusercontent.com/leboncoin/paperclip/master/listing.json") else { return  }
        
        let networkService = NetworkDataFetcher(session: URLSession.urlSession)
        networkService.performRequest(url: url) { (result) in
            switch result {
            case .success(let data):
                do{
                    let result = try JSONDecoder().decode([Product].self, from: data)
                    completion(.success(result))
                } catch {
                    //ERROR: if json decoder fails
                    completion(.failure(NSError(domain: "", code: 540)))//invalidResponse
                }
            case .failure(let error):
                //ERROR: if request fails
                completion(.failure(error))
                break
            }
        }
    }
    
    func fetchCategorys(completion: @escaping (Result<[CategoryProduct] , NSError >)->()) {
        
        
        guard let url = URL(string: "https://raw.githubusercontent.com/leboncoin/paperclip/master/categories.json") else { return  }
        
        let networkService = NetworkDataFetcher(session: URLSession.urlSession)
        networkService.performRequest(url: url) { (result) in
            switch result {
            case .success(let data):
                do{
                    let result = try JSONDecoder().decode([CategoryProduct].self, from: data)
                    completion(.success(result))
                } catch {
                    //ERROR: if json decoder fails
                    completion(.failure(NSError(domain: "", code: 540)))//invalidResponse
                }
            case .failure(let error):
                //ERROR: if request fails
                completion(.failure(error))
                break
            }
        }
    }
    
}
