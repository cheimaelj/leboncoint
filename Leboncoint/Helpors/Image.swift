//
//  Image.swift
//  Leboncoint
//
//  Created by celj on 23/04/2023.
//

import UIKit

func convertUrlToImage(url: String) -> UIImageView {
    let imageView = UIImageView(image: UIImage(named: "default-img"))
    if let imageURL = URL(string: url){
        URLSession.shared.dataTask(with: imageURL) { data, response, error in
            if let data = data, let image = UIImage(data: data) {
                DispatchQueue.main.async {
                    imageView.image = image
                }
            }
        }.resume()
        
    }
    return imageView
}
