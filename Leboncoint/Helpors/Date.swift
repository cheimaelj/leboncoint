//
//  Date.swift
//  Leboncoint
//
//  Created by celj on 17/04/2023.
//

import Foundation


func convertDateFormater(date: String) -> String {
    
    let dateFormatterToDate = ISO8601DateFormatter()
    let dateFormatter = DateFormatter()
    
    if let dt = dateFormatterToDate.date(from: date) {
        dateFormatter.timeZone = TimeZone.current
        if Locale.is24Hour{
            dateFormatter.dateFormat = "dd/MM/YY HH'h'mm"
        }else{
            dateFormatter.dateFormat = "dd/MM/YY hh':'mm a"
        }
        return dateFormatter.string(from: dt)
    }else{
        return "unknow date"
    }
}

func convertStringToDate(dateString: String) -> Date{
    
    let dateFormatter = DateFormatter()
    
    let date = dateFormatter.date(from: dateString)
    if let date = date {
        return date
    } else {
       return Date()
    }
}


extension Locale {
    static var is24Hour: Bool {
        let dateFormat = DateFormatter.dateFormat(fromTemplate: "j", options: 0, locale: Locale.current)!
        return dateFormat.firstIndex(of: "a") == nil
    }
}
