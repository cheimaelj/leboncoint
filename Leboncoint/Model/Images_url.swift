//
//  Images_url.swift
//  Leboncoint
//
//  Created by celj on 14/04/2023.
//

import Foundation
struct Images_url : Codable {
	let small : String?
	let thumb : String?

	enum CodingKeys: String, CodingKey {

		case small = "small"
		case thumb = "thumb"
	}
    
    var stringArray: [String] {
            return [small ?? "", thumb ?? ""]
    }

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		small = try values.decodeIfPresent(String.self, forKey: .small)
		thumb = try values.decodeIfPresent(String.self, forKey: .thumb)
	}
    init(){
        small = ""
        thumb = ""
    }

}
